What is a database?
 - Database is a collection of organized data. Data is arranged and organized in a meaningful way.

Random Group of Data without any context or meaning:
"Cardo",
"Cancelled",
true,
25000
Corn Flakes,
5

Data vs Information
Data is a raw input and does not hold specific meaning
Information is data arranged to hold a specific meaning.
  - is usually associated with a data and a label.

Mobile Number: 09123456789

{
  name: "Michael Jordan",
  championships: [1991, 1992, 1993, 1996, 1997, 1998],
  draftYear: 1984,
  draftPosition: 3,
  MVPs: 5
}

DMBS - Database Management System
  - allows us to manipulate or manage databases. We could perform CRUD operations on our database

What is CRUD?
  - Basic operations to manipulate and manage a database
  C - Create - to add or insert an entry into the database
  R - Read/Retrieve - to select and get an entry from a database
  U - Update - to select and update an entry from a database
  D - Delete - to simply select and delete an entry from a database

Relational Databases
  - allow us to store data and information and arrange them meaningfully in a table
  * data is arranged and stored in Tables with Columns and Rows

    Subject 1stQuarter 2ndQuarter 3rdQuarter 4thQuarter
    
    Math    75         76        89          80


    Relational Database for an E-commerce website

    Products Tables
    id    name    price
    1     PS5     27500
    2     PS4     16000

    SQL => Structured Query Language. It is typically used to programmatically manage or manipulate a relational database

    NoSQL => Not Only Structured Query Language
      Data is arranged and organized in JSON format
      Data is arranged and organized in a more complex and scalable format

    [
      {
        "id": 123-0123,
        "name": "PS5",
        "price": 27500
      },
      {
        "id": 123-0124,
        "name": "PS4",
        "price": 16000
      }
    ]
                                    SQL     NoSQL
    A collection or group of data   Table   Collection
    An entry of data:               Rows    Document
    Defines the values of an entry: Column  Fields

    SQL vs NoSQL: Use Cases

    SQL/Relational Database - data and its structure changes less often and need not be changed immediately and dynamically
      - most banks/financial institutions and use RDBMS

    NoSQL - data and structure changes often and is very dynamically

    Twitter Post 
    {
      id: 1,
      user:
      content:
      date:
      likes:
      retweets:
      comments: [
        comment:{
          commentid:
          user:
          content:
          hashtags:
          likes:
          retweets:
          comments:[
            {
              commentid:
              user:
              content:
              hashtags:
              likes:
            }
          ]
        }
      ]
    }

    MERN - The letter "M" is MERN stack:
    MongoDB
    The reason why MongoDB is called Mongo - humongous
      - referes to the way that in MongoDB, data structure can become huge
      - is the leading NoSQL database
    MongoDB Atlas - the cloud/online service or MongoDB database